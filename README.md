# RevAssess-Ingestion-Service

## Project Description

The RevAssess Injestion Service is responsible for accepting assessment JSONs, verifying their existence in the database, and publishing them to the Pub/Sub topic. It also logs any errors or successful publishes in BigQuery using Bunyan Logging.

## Technologies Used

- Node.js 14
- Pub/Sub
- PostgreSQL
- Bunyan Logging
- BigQuery

## Features

- Recieve an assessment JSON, verify it in the SQL database, and publish it to the Pub/Sub topic
- Log errors and successful publishes in BigQuery using Bunyan
- Communicates with the REST API to interact with the SQL database
