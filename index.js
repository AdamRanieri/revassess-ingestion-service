const {PubSub} = require("@google-cloud/pubsub");
const cors = require ('cors')
const express = require ('express')
const {LoggingBunyan} = require('@google-cloud/logging-bunyan');
const bunyan = require('bunyan')
const axios = require('axios')

const app = express();
app.use(cors())
app.use(express.json({limit: '200mb'}))
app.use(express.urlencoded({limit: '200mb', extended: true}))

const pubsub = new PubSub({projectId:"revassess-dev"});
const cloudLogger = new LoggingBunyan();

const config = {
    name:'revassess-dev', 
    //streams are where you log the data
    streams:[
        {stream:process.stdout, level:'info'},
        cloudLogger.stream('info')
    ]
}
const logger = bunyan.createLogger(config)

app.post('/assessment', async (req, res) =>{

    let assessment = req.body;

    try {
        const result = await axios.get(`http://34.70.99.158/ra/exercises/${assessment.exerciseId}/verify`);
        assessment.assessmentTime = Date.now();
        const response = await pubsub.topic('revassess-topic').publishJSON(assessment); 
        logger.info("Assessment submitted successfully.");
        res.status(201).send("Assessment submitted successfully.");
    } catch {
        logger.error(`There was an issue verifying the exercise with Id ${assessment.exerciseId}`);
        res.status(403).send(`There was an issue verifying the exercise with Id ${assessment.exerciseId}`);
    }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, ()=>{console.log(`Assessment ingestion started on port ${PORT}`)});